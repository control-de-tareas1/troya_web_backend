// Se crea un objeto que contendra todas las funciones que se van a exportar
const tareasCtrl = {};

// Export de librerias 
const nodemailer = require('nodemailer');

// Cuando se ejecuta 'npm run dev' se asigna una variable a NODE_ENV, si esta variable es diferente de 'produccion', se usara dotenv para consumir las variables de entorno
if(process.env_NODE_ENV !== 'production'){
    require('dotenv').config();
}

// Export de objetos 
const Tarea = require('../models/Tarea');
const SubTarea = require('../models/SubTarea');
const Usuario = require('../models/Usuario');


// =========================== OBETENER TAREAS POR RESPONSABLE ================================
tareasCtrl.renderTareas = async (req, res) => {
    // Busca las tareas todas las tareas que contengan como responsable el id que recibe
    const tareas = await Tarea.find({
        Responsable: req.body._id
    });

    // Devuelve todas las tareas encontradas en formato JSON
    res.json(tareas);
}


// ============================== OBETENER TAREAS ============================================
tareasCtrl.getAllTareas = async (req, res) => {
    const tareas = await Tarea.find();
    res.json(tareas);
}


// =========================== CREAR NUEVA TAREA ============================================
tareasCtrl.createNewTarea = async (req, res) => {

    const responsable = await Usuario.findOne({
        _id: req.body.Responsable
    });

    const fechaActual = new Date();

    const NombreTarea = req.body.NombreTarea.trim();
    const Descripcion = req.body.Descripcion.trim();
    const Responsable = req.body.Responsable;
    const Creador = req.body.Creador;
    const NombreResponsable = String(responsable.NombreUsuario + " " + responsable.ApellidoUsuario);
    const FechaTermino = req.body.FechaTermino;
    const TareaPadre = req.body.TareaPadre;
    const AceptarRechazar = req.body.AceptarRechazar;
    const Justificacion = req.body.Justificacion;
    const FechaInicio = new Date(`${fechaActual.getMonth()+1}-${fechaActual.getDate()}-${fechaActual.getFullYear()}`);
    const IdFlujo = req.body.IdFlujo;
    const Problema = req.body.Problema;
    const Posicion = req.body.Posicion;
    const Terminada = false;
    const Estado = 'No Iniciado';

    

    if (!NombreTarea || !Descripcion || !Responsable || !FechaTermino) {
        return res.status(401).send('Todos los campos son requeridos')
    }

    const newTarea = new Tarea({
        NombreTarea,
        Descripcion,
        Responsable,
        Creador,
        NombreResponsable,
        FechaInicio,
        FechaTermino,
        TareaPadre,
        AceptarRechazar,
        Justificacion,
        IdFlujo,
        Problema,
        Posicion,
        Terminada,
        Estado
    });

    await newTarea.save();

    res.status(200).send("Tarea Creada exitosamente");
}



// =========================== CREAR NUEVA SUB-TAREA ============================================
tareasCtrl.createNewSubTarea = async (req, res) => {

    const NombreTarea = req.body.NombreTarea.trim();
    const Descripcion = req.body.Descripcion.trim();
    const Responsable = req.body.Responsable;
    
    const NombreResponsable = String(responsable.NombreUsuario + " " + responsable.ApellidoUsuario);
    const FechaTermino = req.body.FechaTermino;
    const TareaPadre = req.body.TareaPadre;
    const AceptarRechazar = req.body.AceptarRechazar;
    const Justificacion = req.body.Justificacion;
    const FechaInicio = req.body.FechaInicio;
    const NombreFlujo = req.body.NombreFlujo;
    const Problema = req.body.Problema;

    if (!NombreTarea || !Descripcion || !Responsable || !FechaTermino) {
        return res.status(401).send('Todos los campos son requeridos')
    }
    const newSubTarea = new SubTarea({
        NombreTarea,
        Responsable,
        Descripcion,
        NombreResponsable,
        FechaTermino,
        TareaPadre,
        AceptarRechazar,
        Justificacion,
        FechaInicio,
        NombreFlujo,
        Problema
    });

    console.log(newSubTarea)
    await newSubTarea.save();
    res.status(200).send("SubTarea Creada exitosamente");
}

// =========================== ELIMINAR TAREA POR ID============================================
tareasCtrl.eliminarTarea = async (req, res) => {
    await Tarea.findByIdAndDelete(req.params._id);
    res.status(200).send("Tarea Eliminada correctamente");
}

// =========================== OBTENER TAREA POR ID ============================================
tareasCtrl.obtenerTarea = async (req, res) => {
    const tarea = await Tarea.findById(req.params.id);
    res.status(200).json(tarea);
}

tareasCtrl.editarTarea = async (req, res) => {
    const NombreTarea = req.body.NombreTarea.trim();
    const Descripcion = req.body.Descripcion.trim();
    const Responsable = req.body.Responsable;
    const FechaTermino = req.body.FechaTermino;
    const TareaPadre = req.body.TareaPadre;
    const AceptarRechazar = req.body.AceptarRechazar;
    const Justificacion = req.body.Justificacion;
    const FechaInicio = req.body.FechaInicio;
    const NombreFlujo = req.body.NombreFlujo;
    const Problema = req.body.Problema;

    await Tarea.findByIdAndUpdate(req.body._id, {
        NombreTarea,
        Responsable,
        Descripcion,
        FechaTermino,
        TareaPadre,
        AceptarRechazar,
        Justificacion,
        FechaInicio,
        NombreFlujo,
        Problema
    });
    res.status(200);
}

// ============================================ ENVIAR MAIL DE RECHAZO ============================================
tareasCtrl.mailRechazo = async (req, res) => {

    const _id = req.body._id;
    const message = req.body.message;
   
    const tarea = await Tarea.findById(_id);
    const responsable = await Usuario.findOne({_id: tarea.Responsable});
    const creador = await Usuario.findOne({_id: tarea.Creador});

    contentHTML = `
    Tarea Rechazada
    
        Nombre: ${tarea.NombreTarea}
        Descripcion: ${tarea.Descripcion}
        Fecha Plazo: ${tarea.FechaTermino.getDate()}-${tarea.FechaTermino.getMonth()+1}-${tarea.FechaTermino.getFullYear()}
        
    

    ${responsable.NombreUsuario} ${responsable.ApellidoUsuario} ha rechazado esta tarea con el siguiente motivo:
    
    ${message}`;


    const transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 26,
        secure: false,
        auth: {
            user: process.env.EMAIL,
            pass: process.env.PASSWORD
        },
        tls: {
            rejectUnauthorized: false
        }
    });
    let opcionesMail = {
        from: "troya.acropolix@gmail.com",
        to: creador.CorreoUsuario,
        subject: 'T R O Y A - Tarea Rechaza',
        text: contentHTML,
        /*  attachments: [{ 
            filename: "file.pdf",
            path: "C:/Users/Username/Desktop/somefile.pdf",
            contentType: "application/pdf"
        }] */
    }
    
    const info = await transporter.sendMail(opcionesMail, function (err, data) {
        if (err) {
            console.log('Error', err);
        } else {
            console.log('todo bien');
        }
    });
    res.status(200).send('enviado');
}

// =========================== CALCULAR DIAS RESTANTES PARA FECHA DE TERMINO DE TAREA ===================================
tareasCtrl.calcularAvance = async (req, res) => {
    const tarea = await Tarea.findById(req.body._id);
    fechaActual = new Date();
    const fechaTermino = new Date(tarea.FechaTermino);
    const fechaInicio = new Date(tarea.FechaInicio);
    let diasRestantes = Math.round(fechaTermino-fechaInicio)/(1000*60*60*24);
    res.status(200).send(diasRestantes.toString());
}

// ===================================================== TERMINAR TAREA =================================================
tareasCtrl.terminarTarea = async (req,res) => {
    console.log(req.body);
    await Tarea.findByIdAndUpdate(req.body._id, {
        Terminada: true
    });
    res.status(200).send('Tarea terminada');
}

// =========================== CAMBIAR ESTADO (NO INICIADO - EN PROCESO - TERMINADO) ====================================
tareasCtrl.cambiarEstado = async (req, res) => {
   
    await Tarea.findByIdAndUpdate(req.body._id, {
        Estado: req.body.estado
    });
    res.status(200).send('Estado actualizado correctamente');
}



module.exports = tareasCtrl;