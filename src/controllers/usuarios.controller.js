const usuariosCtrl = {};


const Usuario = require('../models/Usuario');
const jwt = require('jsonwebtoken');

usuariosCtrl.registrarUsuario = async (req, res) => {
  const {
    NombreUsuario,
    ApellidoUsuario,
    CorreoUsuario,
    ContrasenaUsuario,
    DepartamentoUsuario
  } = req.body;


  const newUsuario = new Usuario({
    NombreUsuario,
    ApellidoUsuario,
    CorreoUsuario,
    ContrasenaUsuario: await Usuario.encryptPassword(ContrasenaUsuario),
    DepartamentoUsuario
  });

  await newUsuario.save();

  const token = jwt.sign({
    _id: newUsuario._id
  }, 'secreto');

  res.status(200).json({
    token
  });
}

usuariosCtrl.ingresarUsuario = async (req, res) => {
  const {
    CorreoUsuario,
    ContrasenaUsuario
  } = req.body;

  const usuario = await Usuario.findOne({
    CorreoUsuario: CorreoUsuario
  });

  console.log(usuario);

  if (!usuario) return res.status(401).send("El mail no existe");

  const contrasenaCorrecta = await Usuario.matchPassword(ContrasenaUsuario, usuario.ContrasenaUsuario);

  if (!contrasenaCorrecta) return res.status(401).send("Contraseña incorrecta");


  const token = jwt.sign({
    _id: usuario._id
  }, 'secreto',{ expiresIn: 300});

  return res.status(200).json({
    token
  });
}

usuariosCtrl.listarUsuarioPorDepartamento = async (req, res) => {


  const usuario = await Usuario.findOne({
    _id: req.body._id
  });

  const usuarios = await Usuario.find({
    DepartamentoUsuario: usuario.DepartamentoUsuario
  });

  
  return res.status(200).json(usuarios);

}

module.exports = usuariosCtrl;
