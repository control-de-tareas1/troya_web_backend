const router = require('express').Router();

const {
    authJwt
} = require('../middlewares/index')

const {
    renderTareas,
    createNewTarea,
    eliminarTarea,
    obtenerTarea,
    editarTarea,
    getAllTareas,
    mailRechazo,
    createNewSubTarea,
    calcularAvance,
    terminarTarea,
    cambiarEstado,
    reportarProblema
} = require('../controllers/tareas.controller');

// Helpers
const {
    puedeCrearTarea,
    puedeEliminarTarea,
    puedeEditarTarea,
    puedeVerTarea
} = require("../helpers/auth");



/**
 * @swagger
 *
 * definitions:
 *  Tarea:
 *    type: object
 *    required:
 *      -NombreTarea
 *      -Responsable
 *      -NombreResponsable
 *    properties:
 *      Descripcion:
 *        type: string
 *      NombreTarea:
 *        type: string
 *      TareaPadre:
 *        type: string
 *      AceptarRechazar:
 *        type: boolean
 *        example: null
 *      Justificacion:
 *        type: string
 *      Responsable:
 *        type: string
 *      NombreResponsable:
 *        type: string
 *      FechaInicio:
 *        type: string
 *      FechaTermino:
 *        type: string
 *      NombreFlujo:
 *        type: string
 *      Problema:
 *        type: string
 *      TareaPredecesora:
 *        type: string 
 *  Mail:
 *    type: object
 *    required:
 *      -Nombre
 *      -Email
 *      -Mensaje
 *    properties:
 *      Nombre:
 *        type: string
 *      Email:
 *        type: string
 *      Mensaje:
 *        type: string
 *      Adjunto:
 *        type: application/pdf
 */




/**
 * @swagger
 *
 * /api/tareas:
 *  post:
 *    summary: Devuelve todas las tareas por responsable
 *    description: recibe por parametro un id (del responsable de las tareas a mostrar) y renderiza todas las tareas de ese usuario
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: _id
 *        description: id del usuario al que se le renderizaran las tareas (o subtareas) de las cuales él es el responsable
 *        in:  body
 *        required: true
 *        type: string
 *    responses:
 *      200:
 *        description: tareas
 *        schema:
 *          type: array
 *          items:
 *            $ref: '#/definitions/Tarea'
 *    tags:
 *      - tareas
 */
router.post('/tareas',[puedeVerTarea],renderTareas);

/**
 * @swagger
 *
 * /api/getAllTareas:
 *  get:
 *    summary: Devuelve todas las tareas de la bd 
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: array de tareas
 *        schema:
 *          type: array
 *          items:
 *            $ref: '#/definitions/Tarea'
 *    tags:
 *      - tareas
 */
router.get('/getAllTareas', getAllTareas);


/**
 * @swagger
 *
 * /api/obtener-tarea/(id):
 *  get:
 *    summary: Recibe por parametro el id de una tarea y devuelve sus datos
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: id
 *        description: id de la tarea
 *        in:  path
 *        required: true
 *        type: string
 *    responses:
 *      200:
 *        description: tarea encontrada
 *        schema:
 *          $ref: '#/definitions/Tarea'
 *    tags:
 *      - tareas
 */
router.get('/obtener-tarea/:id', obtenerTarea);


/**
 * @swagger
 *
 * /api/add-tarea:
 *  post:
 *    summary: agrega tarea
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: tarea agregada
 *        schema:
 *          $ref: '#/definitions/Tarea'
 *    tags:
 *      - tareas
 */
router.post('/add-tarea',[puedeCrearTarea], createNewTarea);

/**
 * @swagger
 *
 * /api/add-subtarea:
 *  post:
 *    summary: agrega sub tarea
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: subtarea agregada
 *        schema:
 *          $ref: '#/definitions/Tarea'
 *    tags:
 *      - tareas
 */
router.post('/add-subtarea', createNewSubTarea);



/**
 * @swagger
 *
 * /api/eliminar-tarea/(id):
 *  delete:
 *    summary: recibe por parametro un id y elimina la tarea que tenga ese id
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: id
 *        description: id de la tarea
 *        in:  path
 *        required: true
 *        type: string
 *    responses:
 *      200:
 *        description: tarea eliminada
 *        schema:
 *          $ref: '#/definitions/Tarea'
 *    tags:
 *      - tareas
 */
router.delete('/eliminar-tarea/:_id',[puedeEliminarTarea], eliminarTarea);


/**
 * @swagger
 *
 * /api/editar-tarea:
 *  put:
 *    summary: recibe una tarea y  actualiza sus datos
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: tarea
 *        description: tarea a ser editada
 *        in:  body
 *        required: true
 *        schema:
 *          $ref: '#/definitions/Tarea'
 *    responses:
 *      200:
 *        description: tarea actualizada
 *        schema:
 *          $ref: '#/definitions/Tarea'
 *    tags:
 *      - tareas
 */
router.put('/editar-tarea/',[puedeEditarTarea], editarTarea);


/**
 * @swagger
 *
 * /api/mail-rechazo:
 *  post:
 *    summary: envia por un correo
 *    parameters:
 *      - name: nombre
 *        description: el titulo del email
 *        in:  body
 *        required: true
 *        type: string
 *      - name: descripcion
 *        description: descripcion de la tarea (tarea.descripcion)
 *        in:  body
 *        required: true
 *        type: string
 *      - name: email
 *        description: email del usuario al que sera enviado 
 *        in:  body
 *        required: true
 *        type: string
 *      - name: message
 *        description: cuerpo del mensaje que sera enviado
 *        in:  body
 *        required: true
 *        type: string
 *      - name: FechaTermino
 *        description: fecha de termino de la tarea
 *        in:  body
 *        required: true
 *        type: Date
 *      - name: Responsable
 *        description: resonsable de la tarea que se va a rechazar
 *        in:  body
 *        required: true
 *        type: string
 *      - name: adjunto
 *        description: archivo pdf que sera adjuntado
 *        in:  body
 *        required: false
 *        schema:
 *          type: application/pdf
 *    responses:
 *      200:
 *        description: mail enviado
 *    tags:
 *      - tareas
 */
router.post('/mail-rechazo', mailRechazo);



/**
 * @swagger
 *
 * /api/calcular-avance:
 *  post:
 *    summary: devuelve la cantidad de dias restantes
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: FechaTermino
 *        description: fecha en la que termina una tarea
 *        in:  body
 *        required: true
 *        type: Date
 *      - name: FechaInicio
 *        description: fecha en la que empezo la tarea
 *        in:  body
 *        required: true
 *        type: Date
 *    responses:
 *      200:
 *        description: devuelve el numero de dias restantes en formato string
 *    tags:
 *      - tareas
 */
router.post('/calcular-avance',calcularAvance);


/**
 * @swagger
 *
 * /api/terminar-tarea:
 *  put:
 *    summary: setea el campo 'Terminado' de una tarea a 'true'
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: _id
 *        description: id de la tarea que se termino
 *        in:  body
 *        required: true
 *        type: ObjectId
 *    responses:
 *      200:
 *        description: tarea terminada exitosamente
 *    tags:
 *      - tareas
 */
router.put('/terminar-tarea',terminarTarea);

/**
 * @swagger
 *
 * /api/terminar-tarea:
 *  put:
 *    summary: cambia el estado de una tarea
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: _id
 *        description: id de la tarea a la que se le cambiara el estado
 *        in:  body
 *        required: true
 *        type: ObjectId
 *      - name: estado
 *        description: el estado que se le asignara a la tarea
 *        in:  body
 *        required: true
 *        type: string
 *    responses:
 *      200:
 *        description: estado cambiado exitosamente
 *    tags:
 *      - tareas
 */
router.put('/cambiar-estado',cambiarEstado);

module.exports = router