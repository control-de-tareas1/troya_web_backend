const router = require('express').Router();

const {
    Main
} = require('../controllers/index.controller');

router.get('/', Main);

module.exports = router