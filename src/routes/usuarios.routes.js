const router = require("express").Router();

const {
    registrarUsuario,
    ingresarUsuario,
    listarUsuarioPorDepartamento
} = require("../controllers/usuarios.controller");

/**
 * @swagger
 *
 * definitions:
 *  Usuario:
 *    type: object
 *    required:
 *      -NombreUsuario
 *      -ApellidoUsuario
 *      -CorreoUsuario
 *      -RolUsuario
 *      -ContrasenaUsuario
 *      -DepartamentoUsuario
 *    properties:
 *      NombreUsuario:
 *        type: string
 *      ApellidoUsuario:
 *        type: string
 *      CorreoUsuario:
 *        type: string
 *      RolUsuario:
 *        ref: Role
 *        type: Object
 *      ContrasenaUsuario:
 *        type: string
 *      DepartamentoUsuario:
 *        type: string
 *      
 */





/**
 * @swagger
 *
 * /api/signup:
 *  post:
 *    summary: registra un usuario
 *    description: recibe por parametro los datos de un usuario y lo registra en la base de datos
 *    parameters:
 *      - name: usuario
 *        description: objeto con los datos del usuario
 *        in:  body
 *        required: true
 *        schema:
 *          $ref: '#/definitions/Usuario'
 *    responses:
 *      200:
 *        description: usuario
 *    tags:
 *      - usuarios
 */
router.post('/signup', registrarUsuario);




/**
 * @swagger
 *
 * /api/signin:
 *  post:
 *    summary: loguea a un usuario
 *    description: recibe por parametro las credenciales de un usuario y devuelve un token de acceso
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: email
 *        description: email del usuario a ingresar
 *        in:  body
 *        required: true
 *        type: string
 *      - name: contrasena
 *        description: contrasena del usuario a ingresar
 *        in:  body
 *        required: true
 *        type: string
 *    responses:
 *      200:
 *        description: usuario ingresado, devuelve token
 *        schema:
 *          type: string
 *    tags:
 *      - usuarios
 */
router.post('/signin', ingresarUsuario);

/**
 * @swagger
 *
 * /api/listarUsuariosPorDepartamento:
 *  post:
 *    summary: lista usuarios por departamento
 *    description: recibe el id de un usuario lista todos los usuarios del mismo departamento que tiene el usuario dado
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: _id
 *        description: id del usuario
 *        in:  body
 *        required: true
 *        type: string
 *    responses:
 *      200:
 *        description: usuarios
 *        schema:
 *          type: array
 *          items:
 *            $ref: '#/definitions/Usuario'
 *    tags:
 *      - usuarios
 */
router.post('/listarUsuariosPorDepartamento',listarUsuarioPorDepartamento)

module.exports = router;