const express = require('express');
const app = express();
const cors = require('cors');
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: "TROYA API",
            description: "informacion sobre la API de la aplicacion TROYA WEB"
        }
    },
    apis: [(__dirname + "/routes/tareas.routes.js"), (__dirname + "/routes/usuarios.routes.js")]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));


app.use(cors());
app.use(express.json());

// Rutas
app.use('/api',require('./routes/usuarios.routes'));
app.use('/api',require('./routes/tareas.routes'));
app.use('/', require('./routes/index.routes'));

module.exports = app;