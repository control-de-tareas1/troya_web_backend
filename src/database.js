const mongoose = require('mongoose');
if(process.env_NODE_ENV !== 'production'){
  require('dotenv').config();
}

/* mongoose.connect('mongodb+srv://felipe:felipe2020@troya.gnddo.gcp.mongodb.net/ControlTareas?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true
  })
  .then(db => console.log('Database is Connected'))
  .catch(err => console.log(err)); */


  mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true
  })
  .then(db => console.log('Database is Connected'))
  .catch(err => console.log(err));