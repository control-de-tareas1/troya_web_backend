const jwt = require("jsonwebtoken");
const Usuario = require("../models/Usuario");
const Rol = require("../models/Role");


const verifyToken = (req, res, next) => {

    if (!req.headers.authorization) return res.status(401).send('No Autorizado');

    const token = req.headers.authorization.split(' ')[1];
    if (token === 'null') return res.status(401).send('No Autorizado');

    payload = jwt.verify(token, 'secreto');

    req.usuarioId = payload._id;
    next();
};


module.exports = {
    verifyToken
}