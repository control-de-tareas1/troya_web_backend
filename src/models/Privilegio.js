const {
    Schema,
    model
} = require('mongoose')

const privilegioSchema = new Schema({
    NombrePrivilegio: {
        type: String
    }
});

module.exports = model('Privilegio', privilegioSchema)