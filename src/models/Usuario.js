const {
    Schema,
    model
} = require('mongoose')
 const bcrypt = require("bcrypt"); 

const usuarioSchema = new Schema({

    NombreUsuario: {
        type: String,
    },
    ApellidoUsuario: {
        type: String,
    },
    CorreoUsuario: {
        type: String,
        required: true,
        unique: true
    },
    RolUsuario: {
        ref: "Role",
        type: Object
    },
    ContrasenaUsuario: {
        type: String,
        required: true
    },
    DepartamentoUsuario: {
        type: String
    },
    CargaTrabajo: {
        type: String
    }
})

usuarioSchema.statics.encryptPassword = async function (ContrasenaUsuario)  {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(ContrasenaUsuario, salt);
};

usuarioSchema.statics.matchPassword = async function (ContrasenaUsuario,ContrasenaRecibida) {
    return await bcrypt.compare(ContrasenaUsuario, ContrasenaRecibida);
};


module.exports = model('Usuario', usuarioSchema);