const {
    Schema,
    model
} = require('mongoose')

const roleSchema = new Schema({
    NombreRol: {
        type: String
    },
    Privilegios:{
        ref: "Privilegio",
        type: Array
    }
});

module.exports = model('Role', roleSchema)