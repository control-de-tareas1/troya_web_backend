const {
    Schema,
    model
} = require('mongoose')

const TareaSchema = new Schema({

    NombreTarea: {
        type: String,
        required: true
    },
    Descripcion: {
        type: String
    },
    Responsable: {
        type: String,
        required: true
    },
    Creador: {
        type: String
    },
    NombreResponsable: {
        type: String,
        required: true
    },
    FechaInicio: {
        type: Date
    },
    FechaTermino: {
        type: Date
    },
    TareaPadre: {
        type: String
    },
    AceptarRechazar: {
        type: Boolean
    },
    Justificacion: {
        type: String
    },
    IdFlujo: {
        type: String
    },
    Problema: {
        type: String
    },
    Posicion: {
        type: Number
    },
    Terminada:{
        type: Boolean
    },
    Estado:{
        type: String
    }
})

module.exports = model('Tarea', TareaSchema)