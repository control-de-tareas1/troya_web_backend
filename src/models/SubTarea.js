const {
    Schema,
    model
} = require('mongoose')

const SubTareaSchema = new Schema({

    Descripcion: {
        type: String
    },
    NombreTarea: {
        type: String,
        required: true
    },
    TareaPadre: {
        type: String
    },
    AceptarRechazar: {
        type: Boolean
    },
    Justificacion: {
        type: String
    },
    Responsable: {
        type: String,
        required: true
    },
    NombreResponsable: {
        type: String,
        required: true
    },
    FechaInicio: {
        type: String
    },
    FechaTermino: {
        type: String
    },
    NombreFlujo: {
        type: String
    },
    Problema: {
        type: String
    },
    TareaPredecesora: {
        type: String
    }
})

module.exports = model('SubTarea', SubTareaSchema)