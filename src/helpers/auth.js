const usuariosCtrl = require("../controllers/usuarios.controller");
const jwt = require("jsonwebtoken");
const Usuario = require("../models/Usuario");
const Rol = require("../models/Role");

const helpers = {};


helpers.puedeCrearTarea = async (req, res, next) => {
    try {
       
        const token = req.headers.authorization.split(' ')[1];
        let decoded = jwt.decode(token, 'secreto', true);
        const usuario = await Usuario.findById(decoded._id);
        privs = usuario.toJSON().Roles.Privilegios
        console.log(privs);
        let existePrivilegio = false;
        privs.forEach(privilegio => {
            console.log(privilegio)
            if (privilegio.toLowerCase() === "crear tarea") {
                existePrivilegio = true;
            }
        })
        if (existePrivilegio) {
            return next();
        }
    } catch (error) {
        console.log(error);
    }
    res.status(400).send('no posee privilegios para acceder aqui');
};

helpers.puedeEliminarTarea = async (req, res, next) => {
    try {
        
        const token = req.headers.authorization.split(' ')[1];
        let decoded = jwt.decode(token, 'secreto', true);
        const usuario = await Usuario.findById(decoded._id);
        privs = usuario.toJSON().Roles.Privilegios
        console.log(privs);
        let existePrivilegio = false;
        privs.forEach(privilegio => {
            console.log(privilegio)
            if (privilegio.toLowerCase() === "eliminar tarea") {
                existePrivilegio = true;
            }
        })
        if (existePrivilegio) {
            return next();
        }
    } catch (error) {
        console.log(error);
    }
    res.status(400).send('no posee privilegios para acceder aqui');
};


helpers.puedeEditarTarea = async (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        let decoded = jwt.decode(token, 'secreto', true);
        const usuario = await Usuario.findById(decoded._id);
        privs = usuario.toJSON().Roles.Privilegios
        console.log(privs);
        let existePrivilegio = false;
        privs.forEach(privilegio => {
            console.log(privilegio)
            if (privilegio.toLowerCase() === "editar tarea") {
                existePrivilegio = true;
            }
        })
        if (existePrivilegio) {
            return next();
        }
    } catch (error) {
        console.log(error);
    }
    res.status(400).send('no posee privilegios para acceder aqui');
};

helpers.puedeVerTarea = async (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        let decoded = jwt.decode(token, 'secreto', true);
        const usuario = await Usuario.findById(decoded._id);
        privs = usuario.toJSON().Roles.Privilegios
        console.log(privs);
        let existePrivilegio = false;
        privs.forEach(privilegio => {
            console.log(privilegio)
            if (privilegio.toLowerCase() === "ver tarea") {
                existePrivilegio = true;
            }
        })
        if (existePrivilegio) {
            return next();
        }
    } catch (error) {
        console.log(error);
    }
    res.status(400).send('no posee privilegios para acceder aqui');
};

module.exports = helpers;